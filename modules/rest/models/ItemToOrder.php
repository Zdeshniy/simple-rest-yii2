<?
namespace app\modules\rest\models;

use Yii;

class ItemToOrder extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'item_to_order';
	}

	public function rules(){
		return [
			[
				[
					'item_id',
					'count',
					'color_id'
				],
				'integer'
			],
			[
				[ 'price' ],
				'number'
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'item_id' => 'Элемент',
			'price' => 'Цена',
			'count' => 'Количество',
			'color_id' => 'Цвет'
		];
	}

	public function getColor(){
		return $this->hasOne( Color::className(), [ 'id' => 'color_id' ] );
	}

	public function getItem(){
		return $this->hasOne( Item::className(), [ 'id' => 'item_id' ] );
	}

	public function getOrderItems(){
		return $this->hasMany( OrderItem::className(), [ 'item_id' => 'id' ] );
	}
}
