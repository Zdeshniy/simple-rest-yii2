<?
$params = array_merge( require( __DIR__ . '/params.php' ), require( __DIR__ . '/params-local.php' ) );

return [
	'id' => 'app-console',
	'basePath' => dirname( __DIR__ ),
	'bootstrap' => [
		'log'
	],
	'components' => [
		'log' => [
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => [
						'error',
						'warning'
					]
				]
			]
		]
	],
	'modules' => [
		'main' => [
			'class' => 'app\modules\main\Module'
		],
		'user' => [
			'class' => 'app\modules\user\Module'
		]
	],
	'params' => $params
];
