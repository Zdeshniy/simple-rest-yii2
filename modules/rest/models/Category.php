<?
namespace app\modules\rest\models;

use Yii;

class Category extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'category';
	}

	public function rules(){
		return [
			[
				[
					'name',
					'code'
				],
				'required'
			],
			[
				[ 'code' ],
				'number'
			],
			[
				[ 'name' ],
				'string',
				'max' => 255
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Название',
			'code' => 'Символьный код'
		];
	}

	public function getItems(){
		return $this->hasMany( Item::className(), [ 'category_id' => 'id' ] );
	}
}
