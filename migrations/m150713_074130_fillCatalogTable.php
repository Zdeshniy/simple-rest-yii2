<?
use yii\db\Schema;
use yii\db\Migration;

class m150713_074130_fillCatalogTable extends Migration{
	private $category = [
		[
			'name' => 'Цветы',
			'code' => 'flowers'
		],
		[
			'name' => 'Декор',
			'code' => 'decor'
		],
		[
			'name' => 'Форма',
			'code' => 'forms'
		],
		[
			'name' => 'Оформление',
			'code' => 'figuration'
		]
	];

	private $colors = [
		'желтый',
		'белый',
		'зеленый',
		'синий',
		'фиолеторвый'
	];

	private $items = [
		[
			'name' => 'Хризантема',
			'price' => 70,
			'description' => 'Данный цветок сочетает в себе красоту и многогранность',
			'img' => 'test1.jpg',
			'category_id' => 1,
			'colors' => [ 1, 2, 3, 4, 5 ]
		],
		[
			'name' => 'Роза',
			'price' => 150,
			'description' => 'Данный цветок сочетает в себе красоту и многогранность',
			'img' => 'test2.jpg',
			'category_id' => 1,
			'colors' => [ 1, 2, 3 ]
		],
		[
			'name' => 'Фетр',
			'price' => 170,
			'description' => 'Описание фетра',
			'img' => 'test3.jpg',
			'category_id' => 2,
			'colors' => [ 1, 2, 3, 4, 5 ]
		],
		[
			'name' => 'Круглый',
			'price' => 0,
			'category_id' => 3
		],
		[
			'name' => 'Каскадом',
			'price' => 0,
			'category_id' => 3
		],
		[
			'name' => 'Корзина',
			'price' => 0,
			'category_id' => 3
		],
		[
			'name' => 'Россыпь',
			'price' => 0,
			'category_id' => 3
		],
		[
			'name' => 'Без оформления',
			'price' => 0,
			'category_id' => 4
		],
		[
			'name' => 'Сетка-снег',
			'price' => 400,
			'category_id' => 4
		],
		[
			'name' => 'Органза',
			'price' => 300,
			'category_id' => 4
		]
	];

	public function up(){
		foreach( $this->category as $cat ){
			$this->insert( '{{%category}}', [
				'name' => $cat['name'],
				'code' => $cat['code']
			] );
		}

		foreach( $this->colors as $color ){
			$this->insert( '{{%color}}', [
				'name' => $color
			] );
		}

		foreach( $this->items as $item ){
			$this->insert( '{{%item}}', [
				'name' => $item['name'],
				'price' => $item['price'],
				'description' => $item['description'],
				'img' => $item['img'],
				'category_id' => $item['category_id']
			] );

			$id = $this->db->getLastInsertID();

			if( $item['colors'] ){
				foreach( $item['colors'] as $color ){
					$this->insert( '{{%item_color}}', [
						'item_id' => $id, 'color_id' => $color
					] );
				}
			}
		}
	}

	public function down(){
		$this->dropForeignKey( 'FK_item_color_item', '{{%item_color}}' );
		$this->dropForeignKey( 'FK_item_color_color', '{{%item_color}}' );

		$this->truncateTable( '{{%item}}' );

		$this->truncateTable( '{{%item_color}}' );

		$this->truncateTable( '{{%color}}' );

		$this->addForeignKey( 'FK_item_color_item', '{{%item_color}}', 'item_id', '{{%item}}', 'id', 'CASCADE', 'CASCADE' );
		$this->addForeignKey( 'FK_item_color_color', '{{%item_color}}', 'color_id', '{{%color}}', 'id', 'CASCADE', 'CASCADE' );

		$this->dropForeignKey( 'FK_catalog_item', '{{%item}}' );

		$this->truncateTable( '{{%category}}' );

		$this->addForeignKey( 'FK_catalog_item', '{{%item}}', 'category_id', '{{%category}}', 'id', 'SET NULL', 'CASCADE' );
	}
}
