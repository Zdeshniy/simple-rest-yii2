<?
namespace app\modules\rest\models;

use Yii;
use voskobovich\behaviors\ManyToManyBehavior;

class Item extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'item';
	}

	public function rules(){
		return [
			[
				[
					'name',
					'price'
				],
				'required'
			],
			[
				[ 'price' ],
				'number'
			],
			[
				[ 'category_id' ],
				'integer'
			],
			[
				[
					'name',
					'description',
					'img'
				],
				'string',
				'max' => 255
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Название',
			'price' => 'Цена',
			'description' => 'Описание',
			'img' => 'Изображение',
			'category_id' => 'Категория',
		];
	}

	public function getAttributes( $names = null, $except = [ ] ){
		$attributes = parent::getAttributes( $names = null, $except = [ ] );

		return array_replace( $attributes, [
			'colors' => $this->colors
		] );
	}

	public function behaviors(){
		return [
			[
				'class' => ManyToManyBehavior::className(),
				'relations' => [
					'colors' => 'colors'
				]
			]
		];
	}

	public function getCategory(){
		return $this->hasOne( Category::className(), [ 'id' => 'category_id' ] );
	}

	public function getColors(){
		return $this->hasMany( Color::className(), [ 'id' => 'color_id' ] )
			->viaTable( 'item_color', [
				'item_id' => 'id'
			] );
	}
}
