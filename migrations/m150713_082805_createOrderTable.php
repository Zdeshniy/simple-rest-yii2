<?
use yii\db\Schema;
use yii\db\Migration;

class m150713_082805_createOrderTable extends Migration{
	public function up(){
		$tableOptions = NULL;
		if( $this->db->driverName === 'mysql' ){
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable( '{{%order}}', [
			'id' => Schema::TYPE_PK,
			'price' => Schema::TYPE_DOUBLE . ' NOT NULL'
		], $tableOptions );

		$this->createTable( '{{%item_to_order}}', [
			'id' => Schema::TYPE_PK,
			'item_id' => Schema::TYPE_INTEGER,
			'price' => Schema::TYPE_DOUBLE,
			'count' => Schema::TYPE_INTEGER,
			'color_id' => Schema::TYPE_INTEGER
		], $tableOptions );

		$this->createIndex( 'IX_item_order_item', '{{%item_to_order}}', 'item_id' );
		$this->addForeignKey( 'FK_item_order_item', '{{%item_to_order}}', 'item_id', '{{%item}}', 'id', 'SET NULL', 'CASCADE' );

		$this->createIndex( 'IX_item_order_color', '{{%item_to_order}}', 'color_id' );
		$this->addForeignKey( 'FK_item_order_color', '{{%item_to_order}}', 'color_id', '{{%color}}', 'id', 'SET NULL', 'CASCADE' );

		$this->createTable( '{{%order_item}}', [
			'id' => Schema::TYPE_PK,
			'order_id' => Schema::TYPE_INTEGER,
			'item_id' => Schema::TYPE_INTEGER
		], $tableOptions );

		$this->createIndex( 'IX_order_item_order', '{{%order_item}}', 'order_id' );
		$this->addForeignKey( 'FK_order_item_order', '{{%order_item}}', 'order_id', '{{%order}}', 'id', 'CASCADE', 'CASCADE' );

		$this->createIndex( 'IX_order_item_item', '{{%order_item}}', 'item_id' );
		$this->addForeignKey( 'FK_order_item_item', '{{%order_item}}', 'item_id', '{{%item_to_order}}', 'id', 'CASCADE', 'CASCADE' );
	}

	public function down(){
		$this->dropTable( '{{%order_item}}' );

		$this->dropTable( '{{%item_to_order}}' );

		$this->dropTable( '{{%order}}' );
	}
}
